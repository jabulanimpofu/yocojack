const tests = require('./tests.json');
const YocoJack = require('./yokoJack').YocoJack

test('Results equal to test data', function () {
    for (var i = 0; i < tests.length; i += 1) {
        var gameData = tests[i];
        var playerACards = gameData.playerA.map(function (card) {
            return {
                name: card.slice(0, card.length - 1),
                suit: card.slice(card.length - 1),
            }
        })
        var playerBCards = gameData.playerB.map(function (card) {
            return {
                name: card.slice(0, card.length - 1),
                suit: card.slice(card.length - 1),
            }
        })
        var game = new YocoJack(playerACards, playerBCards);
        expect(game.playerAWins()).toBe(gameData.playerAWins);
    }
});
