function YocoJack(player1Cards, player2Cards) {
    this.deck = new Shoe(player1Cards, player2Cards);
    this.players = [
        new Player('playerA', this.deck, player1Cards),
        new Player('playerB', this.deck, player2Cards),
    ]
}

YocoJack.prototype.playerAWins = function (index, use_suits) {
    var playerAScore = this.players[0].score(index, use_suits);
    var playerBScore = this.players[1].score(index, use_suits);

    if (playerAScore < 22 && (playerAScore > playerBScore || playerBScore > 21)) {
        return true
    }

    if (playerAScore === playerBScore) {
        index = index ? index + 1 : 1;
        if (!use_suits && ( index > this.players[0].cards.length || index > this.players[1].cards.length)) {
            index = 1;
            use_suits = true;
        }
        return this.playerAWins(index, use_suits)
    }

    return false
}

function Shoe() {
    const args = Array.from(arguments);
    var drawnCards = args.reduce(
        function (drawnCards, playerCards) {
            playerCards = playerCards.reduce(
                function (cards, card) {
                    cards["" + card.name + card.suit] = true
                    return cards;
                },
                {});

            return Object.assign(drawnCards, playerCards);
        },
        {}
    );
    this.cardsDrawn = Object.keys(drawnCards).length;
    this.cards = [];
    var suits = ["S", "D", "H", "C"];
    var names = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
    for (var suit in suits) {
        for (var name in names) {
            if (!drawnCards["" + names[name] + suits[suit]]) {
                this.cards.push(new Card(names[name], suits[suit]));
            }
        }
    }
}

function Card(name, suit) {
    this.name = name;
    this.suit = suit;
}

Card.prototype.value = function () {
    if (this.name === "J" || this.name === "Q" || this.name === "K") {
        return [10];
    } else if (this.name == "A") {
        return [11];
    } else {
        return [parseInt(this.name, 10)];
    }
};

function Player(name, deck, cards) {
    this.name = name;
    this.deck = deck;
    this.cards = cards.map(function (card) {
        return new Card(card.name, card.suit);
    });
    this.cards = this.cards.sort(function (a, b) {
        var aVal = a.value()[0];
        var bVal = b.value()[0];

        if (aVal === 10 && bVal === 10) {
            aVal = getCardValueByName(a.name)
            bVal = getCardValueByName(b.name)
        }

        if (aVal === bVal) {
            aVal = getCardValueBySuit(a.suit);
            bVal = getCardValueBySuit(b.suit);
        }

        if (aVal > bVal) {
            return -1
        }

        if (aVal < bVal) {
            return 1
        }

        return 0;
    })
}

Player.prototype.score = function (level, suits) {
    var score = 0;

    if (!level) {
        for (var i = 0; i < this.cards.length; i++) {
            var value = this.cards[i].value() // value array
            score += value[0];
        }

        return score;
    }

    if (level && !suits) {
        var cards = this.cards.map(function (card) {
            return card.value()[0];
        })

        return cards[level - 1]
    }

    var cards = this.cards.map(function (card) {
        return getCardValueBySuit(card.suit);
    })
    return cards[level - 1];
};

function getCardValueBySuit(suit) {
    switch (suit) {
        case 'S':
            return 4;
        case 'H':
            return 3;
        case 'C':
            return 2;
        case 'D':
            return 1;
        default:
            return 0;
    }
}

function getCardValueByName(name) {
    switch (name) {
        case 'K':
            return 4;
        case 'Q':
            return 3;
        case 'J':
            return 2;
        case '10':
            return 1;
        default:
            return 0;
    }
}

module.exports = {
    YocoJack: YocoJack,
    Shoe: Shoe,
    Card: Card,
    Player: Player,
}