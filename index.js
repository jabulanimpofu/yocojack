var YocoJack = require('./yokoJack').YocoJack;
var fetch = require('node-fetch');

console.log("Welcome to YocoJack. Fetching test cases...");
fetch('https://s3-eu-west-1.amazonaws.com/yoco-testing/tests.json')
    .then(function (response) {
        console.log("Test cases, starting the test runner...");
        return response.json();
    })
    .then(function (tests) {
        for (var i = 0; i < tests.length; i += 1) {
            var gameData = tests[i];
            var playerACards = gameData.playerA.map(function (card) {
                return {
                    name: card.slice(0, card.length - 1),
                    suit: card.slice(card.length - 1),
                }
            })
            var playerBCards = gameData.playerB.map(function (card) {
                return {
                    name: card.slice(0, card.length - 1),
                    suit: card.slice(card.length - 1),
                }
            })
            var game = new YocoJack(playerACards, playerBCards);

            if (game.playerAWins() === gameData.playerAWins) {
                console.log('result matches test data')
            } else {
                console.log('result does not match test data')
            }
        }
    })
    .catch(function (e) {
        console.log(e);
        console.log('something has gone wrong');
    })